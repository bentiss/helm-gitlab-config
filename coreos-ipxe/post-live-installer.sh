#!/bin/bash

BOOT_DISK=$(lsblk -o label,name -r | grep boot | cut -d ' ' -f 2)

mkdir -p /mnt/boot

mount -t ext4 /dev/$BOOT_DISK /mnt/boot

# bug in grub2 handling of md arrays on equinix:
# grub2 takes 15 minutes to do this test :(
sed -i 's|-d (md/md-boot)/grub2|"a" = "b"|' /mnt/boot/grub2/grub.cfg

umount /mnt/boot
